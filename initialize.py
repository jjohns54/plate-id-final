import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import random
import sys


# Use a service account
cred = credentials.Certificate('plateid-40387-9987da035f9f.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

doc_ref = db.collection(u'Plates')

