from .LicensePlate import LicensePlate

STREET_SUFFIXES = ("Street", "St.", "St", "Avenue", "Ave.", "Ave", "Road", "Rd.", "Rd", "Court",
    "Ct.", "Ct", "Lane", "Ln.", "Ln", "Drive", "Dr.", "Dr", "Circle")

class VehicleOwner:
    def __init__(self, ssn, address, firstName, lastName, plate_id):
        self.ssn = ssn
        self.firstName = firstName
        self.lastName = lastName
        results = self.parse_address(address)
        self.streetAddr = results["streetaddr"]
        self.city = results["city"]
        self.state = results["state"]
        self.zipcode = results["zipcode"]
        self.plate = LicensePlate(plate_id, self.state)

    @staticmethod
    def from_dict(source):
        owner_obj = VehicleOwner(source['SSN'], source['address'], source['firstName'], source['lastName'], source['plate_id'])
        return owner_obj

    def to_dict(self):
        return {
            "ssn": self.ssn,
            "firstName": self.firstName,
            "lastName": self.lastName,
            "street": self.streetAddr,
            "city": self.city,
            "state": self.state,
            "zipcode": self.zipcode
        }

    def __repr__(self):
        return "VehicleOwner(ssn={}, firstName={}, lastName={}, streetAddr={}, city={}, state={}, zipcode={}, plate={})".format(
            self.ssn, self.firstName, self.lastName, self.streetAddr, self.city, self.state, self.zipcode, repr(self.plate))

    def parse_address(self, address):
        # can be in two forms:
        results = {"zipcode": None, "city": None, "state": None, "streetaddr": None}
        # 1. StreetAddr City, State Zipcode
        # 2. City, State Zipcode StreetAddr
        address_split = address.split(" ")
        # check if last index is entirely numeric
        if str.isnumeric(address_split[-1]):
            results["zipcode"] = address_split[-1]
            results["state"] = address_split[-2]
            end_index = 0
            for i in range(len(address_split)):
                if address_split[i] in STREET_SUFFIXES:
                    end_index = i
                    break
            street_addr_list = address_split[:(end_index+1)]
            city_list = address_split[(end_index+1):-2]
            lastWord = city_list[-1][:-1]
            city_list[-1] = lastWord
            results["streetaddr"] = " ".join(street_addr_list)
            results["city"] = " ".join(city_list)
        else:
            zipcode_index = 0
            for i in range(len(address_split)):
                if str.isnumeric(address_split[i]):
                    zipcode_index = i
                    break
            results["zipcode"] = address_split[zipcode_index]
            results["state"] = address_split[zipcode_index-1]
            city_list = address_split[:(zipcode_index-1)]
            lastWord = city_list[-1][:-1]
            city_list[-1] = lastWord
            street_addr_list = address_split[(zipcode_index+1):]
            results["streetaddr"] = " ".join(street_addr_list)
            results["city"] = " ".join(city_list)
        return results
