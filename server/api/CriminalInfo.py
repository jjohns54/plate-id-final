class CriminalInfo:
    def __init__(self, ssn, hasArrestWarrants, isFelon, onProbation):
        self.ssn = ssn
        self.hasArrestWarrants = hasArrestWarrants
        self.isFelon = isFelon
        self.onProbation = onProbation

    @staticmethod
    def from_dict(source):
        return CriminalInfo(source['SSN'], source['hasArrestWarrants'], source['isFelon'], source['onProbation'])

    def to_dict(self):
        return {
            "ssn": self.ssn,
            "hasArrestWarrants": self.hasArrestWarrants,
            "isFelon": self.isFelon,
            "onProbation": self.onProbation
        }

    def __repr__(self):
        return "CriminalInfo(ssn={}, hasArrestWarrants={}, isFelon={}, onProbation={})".format(
            self.ssn, self.hasArrestWarrants, self.isFelon, self.onProbation)
