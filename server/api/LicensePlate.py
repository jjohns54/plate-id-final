class LicensePlate:
    def __init__(self, plate_id, state):
        self.plate_id = plate_id
        self.state = state

    @staticmethod
    def from_dict(source):
        return LicensePlate(source['plate_id'], source['state'])

    def to_dict(self):
        return {"plate_id": self.plate_id, "state": self.state}

    def __repr__(self):
        return "LicensePlate(plate_id={}, state={})".format(self.plate_id, self.state)
