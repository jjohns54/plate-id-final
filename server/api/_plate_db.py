import firebase_admin
from firebase_admin import credentials
import os
import requests
import time

class _plate_db:
    def __init__(self):
        cred = credentials.Certificate('scan/plateid-40387-9987da035f9f.json')
        firebase_admin.initialize_app(cred)
        self.LicensePlate = {'plate_id': None, 'state': None}
        self.CriminalInfo = {'ssn': None, 'hasArrestWarrants': None, 'isFelon': None, 'onProbation': None}
        self.VehicleOwner = {'ssn': None, 'firstName': None, 'lastName': None, 'streetAddr': None, 'city': None,
            'state': None, 'zipcode': None, 'plate_id': None}
        self.OutputInfo = {}
        self.TimeStamp = {}
        self.BasePath = os.path.dirname(os.path.abspath('.')) + '/'
        print(os.path.dirname(os.path.abspath('.')))

    def reset(self):
        self.LicensePlate = {'plate_id': "", 'state': "-"}
        self.CriminalInfo = {'ssn': '', 'hasArrestWarrants': False, 'isFelon': False, 'onProbation': False}
        self.VehicleOwner = {'ssn': '', 'firstName': '', 'lastName': '', 'street': '', 'city': '',
            'state': '-', 'zipcode': ''}
        self.OutputInfo = {}
        self.TimeStamp = {'day': time.strftime("%A, %B %e, %Y", time.localtime()), 'time': time.strftime("%I:%M %p", time.localtime())}

    def getOutputInfo(self):
        return self.OutputInfo

    def getInputInfo(self):
        return {'licenseplate': self.LicensePlate, 'vehicleowner': self.VehicleOwner, 'criminalinfo': self.CriminalInfo}

    def setLicensePlate(self, plate_id='', state='-'):
        self.LicensePlate = {'plate_id': plate_id, 'state': state}
        self.TimeStamp = {'day': time.strftime("%A, %B %e, %Y", time.localtime()), 'time': time.strftime("%I:%M %p", time.localtime())}

    def setCriminalInfo(self, ssn='', hasArrestWarrants=False, isFelon=False, onProbation=False):
        self.CriminalInfo = {'ssn': ssn, 'hasArrestWarrants': hasArrestWarrants, 'isFelon': isFelon, 'onProbation': onProbation}

    def setVehicleOwner(self, ssn='', firstName='', lastName='', street='', city='', state='', zipcode=''):
        self.VehicleOwner = {'ssn': ssn, 'firstName': firstName, 'lastName': lastName, 'street': street, 'city': city, 'state': state,
            'zipcode': zipcode}
