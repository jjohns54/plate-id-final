from firebase_admin import firestore
import random
import sys
import requests
import json

from api.LicensePlate import LicensePlate
from api.VehicleOwner import VehicleOwner
from api.CriminalInfo import CriminalInfo

SERVER_URL = "http://localhost:51077"
RESET_URL = SERVER_URL + "/reset/"
INPUT_URL = SERVER_URL + "/inputinfo/"
LICENSEPLATE_URL = INPUT_URL + "licenseplate/"
CRIMINALINFO_URL = INPUT_URL + "criminalinfo/"
VEHICLEOWNER_URL = INPUT_URL + "vehicleowner/"
NPLATES = 877

PlateTemplates = ["1C8SA49", "GYO9CPC"]


def is_json(resp):
    try:
        json.loads(resp)
        return True
    except ValueError:
        return False

def send_reset():
    r = requests.put(RESET_URL, data=json.dumps({}))
    if not is_json(r.content.decode('utf-8')):
        return False
    resp = json.loads(r.content.decode('utf-8'))
    if resp['status'] != 'success':
        return False
    return True

def query_plate(db, plate_num):
    ref = db.collection(u'Plates')

    plate_obj = None
    query = ref.where(u'plate_id', u'==', plate_num)
    docs = query.stream()

    for doc in docs:
        plate_obj = LicensePlate.from_dict(doc.to_dict())
        break
    return plate_obj

def query_owner(db, plate_obj):
    ref = db.collection(u'VehicleOwners')

    owner_obj = None
    query = ref.where(u'plate_id', u'==', plate_obj.plate_id)
    docs = query.stream()
    for doc in docs:
        #print(doc.to_dict())
        owner_obj = VehicleOwner.from_dict(doc.to_dict())
        break
    return owner_obj

def query_criminal(db, owner_obj):
    ref = db.collection(u'CriminalInfo')

    criminal_obj = None
    query = ref.where(u'SSN', u'==', owner_obj.ssn)
    docs = query.stream()
    for doc in docs:
        #print(doc.to_dict())
        criminal_obj = CriminalInfo.from_dict(doc.to_dict())
        break
    return criminal_obj

def send_info(db_obj, endpoint):
    r = requests.post(endpoint, data=json.dumps(db_obj.to_dict()))

    if not is_json(r.content.decode('utf-8')):
        return False
    resp = json.loads(r.content.decode('utf-8'))
    print(resp)
    if resp['status'] != 'success':
        return False
    return True

def run(plate_num=None):
    if not send_reset():
        return -1, ""

    # connect to database
    db = firestore.client()
    if plate_num is None:
        lineNumber = random.randrange(NPLATES)
        fh = open('scan/plates_list', 'r')
        for i, line in enumerate(fh, 0):
            if i == lineNumber:
                plate_num = line.rstrip()
                print('i = ' + str(i))
                print('plate_num = ' + plate_num)
                break
        fh.close()
        if plate_num is None:
            plate_num = PlateTemplates[random.randrange(2)]

    plate_obj = query_plate(db, plate_num)
    if plate_obj is None:
        return 1, plate_num
    if not send_info(plate_obj, LICENSEPLATE_URL):
        return -1, ""
    print(repr(plate_obj))
    print("")

    owner_obj = query_owner(db, plate_obj)
    if owner_obj is None:
        return 2, plate_num
    if not send_info(owner_obj, VEHICLEOWNER_URL):
        return -1, ""
    print(repr(owner_obj))
    print("")

    criminal_obj = query_criminal(db, owner_obj)
    if owner_obj is not None:
        if not send_info(criminal_obj, CRIMINALINFO_URL):
            return -1, ""
    print(repr(criminal_obj))
    print("")
    return 0, ""


#if __name__ == "__main__":
#    cred = credentials.Certificate('plateid-40387-9987da035f9f.json')
#    firebase_admin.initialize_app(cred)
#    x, y = run()
#    print(x)
#    print(y)
#    if r == 0:
#        sys.exit(0)
#    else:
#        sys.exit(1)
