import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import random
import sys
import os
import requests
import json

sys.path.append(os.path.dirname(sys.path[0]))
__package__ = "scan"

from api.LicensePlate import LicensePlate

def get_plates():
    fh = open('plates_list', 'w+')
    db = firestore.client()
    ref = db.collection(u'Plates')

    plate_obj = None
    s = ""
    plate = ""
    docs = ref.stream()

    for doc in docs:
        plate_obj = LicensePlate.from_dict(doc.to_dict())
        plate = plate_obj.plate_id
        s = plate + "\n"
        fh.write(s)
    fh.close()

if __name__ == "__main__":
    cred = credentials.Certificate('plateid-40387-9987da035f9f.json')
    firebase_admin.initialize_app(cred)
    get_plates()
