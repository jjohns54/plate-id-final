import json
import requests
import cherrypy
import scan.check_plate as check_plate
from api._plate_db import _plate_db

class ScanController:
    def __init__(self, db=None):
        if db is None:
            self.db = _plate_db()
        else:
            self.db = db

    def PUT_SCAN(self):
        output = {'status': 'success'}
        try:
            r, details = check_plate.run()
            print(details)
            print(type(details))
            if r == -1:
                output['status'] = 'error'
                output['message'] = 'Internal Database Error'
            elif r == 1: # license plate not found
                output['status'] = 'success'
                output['message'] = 'License plate ' + details + ' was not found.'
            elif r == 2:
                output['status'] = 'success'
                output['message'] = 'Owner of license plate ' + details + ' was not found in the Vehicle Owners database.'
            elif r == 0:
                output['status'] = 'success'
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
