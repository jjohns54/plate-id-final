import json
import requests
import cherrypy
from api._plate_db import _plate_db

class ResetController:
    def __init__(self, db=None):
        if db is None:
            self.db = _plate_db()
        else:
            self.db = db

    def PUT_RESET(self):
        output = {'status': 'success'}
        try:
            self.db.reset()
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
