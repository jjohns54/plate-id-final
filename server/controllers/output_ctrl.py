import json
import requests
import cherrypy
from api._plate_db import _plate_db

class OutputController:
    def __init__(self, db=None):
        if db is None:
            self.db = _plate_db()
        else:
            self.db = db

    def POST_OUTPUT(self):
        output = {'status': 'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        outputInfo = {}
        try:
            outputInfo['ticketInformation'] = data['ticketInformation']
            outputInfo['defendantAddress'] = data['defendantAddress']
            outputInfo['defendantID'] = data['defendantID']
            outputInfo['background'] = data['background']
            outputInfo['offenses'] = data['offenses']
            outputInfo['timestamp'] = self.db.TimeStamp
            self.db.OutputInfo = outputInfo
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def GET_OUTPUT(self):
        output = {'status': 'success'}
        try:
            output['outputinfo'] = self.db.OutputInfo
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
