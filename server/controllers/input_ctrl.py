import json
import requests
import cherrypy
from api._plate_db import _plate_db

class InputController:
    def __init__(self, db=None):
        if db is None:
            self.db = _plate_db()
        else:
            self.db = db

    def GET_INPUT(self):
        output = {'status': 'success'}
        try:
            output['inputinfo'] = self.db.getInputInfo()
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def POST_LICENSE_PLATE(self):
        output = {'status': 'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        try:
            self.db.setLicensePlate(plate_id=data['plate_id'], state=data['state'])
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def POST_CRIMINAL_INFO(self):
        output = {'status': 'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        try:
            self.db.setCriminalInfo(ssn=data['ssn'], hasArrestWarrants=data['hasArrestWarrants'], isFelon=data['isFelon'], onProbation=data['onProbation'])
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def POST_VEHICLE_OWNER(self):
        output = {'status': 'success'}
        data = cherrypy.request.body.read()
        data = json.loads(data)
        try:
            ssnLast = data['ssn'][-4:]
            print(ssnLast)
            self.db.setVehicleOwner(ssn=ssnLast, firstName=data['firstName'], lastName=data['lastName'], street=data['street'], city=data['city'], state=data['state'], zipcode=data['zipcode'])
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
