import json
import requests
import cherrypy
from api._plate_db import _plate_db

class PathController:
    def __init__(self, db=None):
        if db is None:
            self.db = _plate_db()
        else:
            self.db = db

    def GET_PATH(self):
        output = {'status': 'success'}
        try:
            output['path'] = self.db.BasePath
        except Exception as ex:
            output['status'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
