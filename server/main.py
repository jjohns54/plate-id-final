import cherrypy
import requests
import json
import os
import sys
sys.path.append(os.path.dirname(sys.path[0]))

from api._plate_db import _plate_db
from controllers.reset import ResetController
from controllers.input_ctrl import InputController
from controllers.scan_ctrl import ScanController
from controllers.output_ctrl import OutputController
from controllers.path_ctrl import PathController

class OptionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    db_o = _plate_db()

    # controller objects
    inputCtrl = InputController(db=db_o)
    resetCtrl = ResetController(db=db_o)
    scanCtrl = ScanController(db=db_o)
    outputCtrl = OutputController(db=db_o)
    pathCtrl = PathController(db=db_o)

    # connect options to each endpoint
    dispatcher.connect('reset_options', '/reset/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('input_options', '/inputinfo/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('input_plate_options', '/inputinfo/licenseplate/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('input_criminal_options', '/inputinfo/criminalinfo/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('input_owner_options', '/inputinfo/vehicleowner/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('scan_options', '/scan/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('output_options', '/outputinfo/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))
    dispatcher.connect('path_options', '/path/', controller=OptionsController, action='OPTIONS',
        conditions=dict(method=['OPTIONS']))

    # reset endpoint
    dispatcher.connect('reset_all', '/reset/', controller=resetCtrl, action='PUT_RESET',
        conditions=dict(method=['PUT']))

    # input endpoints
    dispatcher.connect('input_get_all', '/inputinfo/', controller=inputCtrl, action='GET_INPUT',
        conditions=dict(method=['GET']))
    dispatcher.connect('input_post_plate', '/inputinfo/licenseplate/', controller=inputCtrl, action='POST_LICENSE_PLATE',
        conditions=dict(method=['POST']))
    dispatcher.connect('input_post_owner', '/inputinfo/vehicleowner/', controller=inputCtrl, action='POST_VEHICLE_OWNER',
        conditions=dict(method=['POST']))
    dispatcher.connect('input_post_criminal', '/inputinfo/criminalinfo/', controller=inputCtrl, action='POST_CRIMINAL_INFO',
        conditions=dict(method=['POST']))

    # scan endpoint
    dispatcher.connect('put_scan', '/scan/', controller=scanCtrl, action='PUT_SCAN',
        conditions=dict(method=['PUT']))

    # path endpoint
    dispatcher.connect('get_path', '/path/', controller=pathCtrl, action='GET_PATH',
        conditions=dict(method=['GET']))

    # output endpoints
    dispatcher.connect('post_output', '/outputinfo/', controller=outputCtrl, action='POST_OUTPUT',
        conditions=dict(method=['POST']))
    dispatcher.connect('get_output', '/outputinfo/', controller=outputCtrl, action='GET_OUTPUT',
        conditions=dict(method=['GET']))


    conf = {
        'global': {
            'server.socket_host': 'localhost',
            'server.socket_port': 51077
        },
        '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True
        }
    }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == "__main__":
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
