/*

>> kasperkamperman.com - 2018-04-18
>> https://www.kasperkamperman.com/blog/camera-template/

*/

var takeSnapshotUI = createClickFeedbackUI();

var video;
var takePhotoButton;
var toggleFullScreenButton;
var switchCameraButton;
var amountOfCameras = 0;
var currentFacingMode = 'environment';

document.addEventListener('DOMContentLoaded', function(event) {
  // do some WebRTC checks before creating the interface
  DetectRTC.load(function() {
    // do some checks
    if (DetectRTC.isWebRTCSupported == false) {
      alert(
        'Please use Chrome, Firefox, iOS 11, Android 5 or higher, Safari 11 or higher',
      );
    } else {
      if (DetectRTC.hasWebcam == false) {
        alert('Please install an external webcam device.');
      } else {
        amountOfCameras = DetectRTC.videoInputDevices.length;

        initCameraUI();
        initCameraStream();
      }
    }

    console.log(
      'RTC Debug info: ' +
        '\n OS:                   ' +
        DetectRTC.osName +
        ' ' +
        DetectRTC.osVersion +
        '\n browser:              ' +
        DetectRTC.browser.fullVersion +
        ' ' +
        DetectRTC.browser.name +
        '\n is Mobile Device:     ' +
        DetectRTC.isMobileDevice +
        '\n has webcam:           ' +
        DetectRTC.hasWebcam +
        '\n has permission:       ' +
        DetectRTC.isWebsiteHasWebcamPermission +
        '\n getUserMedia Support: ' +
        DetectRTC.isGetUserMediaSupported +
        '\n isWebRTC Supported:   ' +
        DetectRTC.isWebRTCSupported +
        '\n WebAudio Supported:   ' +
        DetectRTC.isAudioContextSupported +
        '\n is Mobile Device:     ' +
        DetectRTC.isMobileDevice,
    );
  });
});

function initCameraUI() {
  video = document.getElementById('video');

  takePhotoButton = document.getElementById('takePhotoButton');
  toggleFullScreenButton = document.getElementById('toggleFullScreenButton');
  switchCameraButton = document.getElementById('switchCameraButton');

  // https://developer.mozilla.org/nl/docs/Web/HTML/Element/button
  // https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_button_role

  takePhotoButton.addEventListener('click', function() {
    takeSnapshotUI();
    takeSnapshot();
  });

  // -- fullscreen part

  // -- switch camera part
  if (amountOfCameras > 1) {
    switchCameraButton.style.display = 'block';

    switchCameraButton.addEventListener('click', function() {
      if (currentFacingMode === 'environment') currentFacingMode = 'user';
      else currentFacingMode = 'environment';

      initCameraStream();
    });
  }

  // Listen for orientation changes to make sure buttons stay at the side of the
  // physical (and virtual) buttons (opposite of camera) most of the layout change is done by CSS media queries
  // https://www.sitepoint.com/introducing-screen-orientation-api/
  // https://developer.mozilla.org/en-US/docs/Web/API/Screen/orientation
  window.addEventListener(
    'orientationchange',
    function() {
      // iOS doesn't have screen.orientation, so fallback to window.orientation.
      // screen.orientation will
      if (screen.orientation) angle = screen.orientation.angle;
      else angle = window.orientation;

      var guiControls = document.getElementById('gui_controls').classList;
      var vidContainer = document.getElementById('vid_container').classList;

      if (angle == 270 || angle == -90) {
        guiControls.add('left');
        vidContainer.add('left');
      } else {
        if (guiControls.contains('left')) guiControls.remove('left');
        if (vidContainer.contains('left')) vidContainer.remove('left');
      }

      //0   portrait-primary
      //180 portrait-secondary device is down under
      //90  landscape-primary  buttons at the right
      //270 landscape-secondary buttons at the left
    },
    false,
  );
}

// https://github.com/webrtc/samples/blob/gh-pages/src/content/devices/input-output/js/main.js
function initCameraStream() {
  // stop any active streams in the window
  if (window.stream) {
    window.stream.getTracks().forEach(function(track) {
      track.stop();
    });
  }

  // we ask for a square resolution, it will cropped on top (landscape)
  // or cropped at the sides (landscape)
  var size = 1280;

  var constraints = {
    audio: false,
    video: {
      width: { ideal: size },
      height: { ideal: size },
      //width: { min: 1024, ideal: window.innerWidth, max: 1920 },
      //height: { min: 776, ideal: window.innerHeight, max: 1080 },
      facingMode: currentFacingMode,
    },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(handleSuccess)
    .catch(handleError);

  function handleSuccess(stream) {
    window.stream = stream; // make stream available to browser console
    video.srcObject = stream;

    if (constraints.video.facingMode) {
      if (constraints.video.facingMode === 'environment') {
        switchCameraButton.setAttribute('aria-pressed', true);
      } else {
        switchCameraButton.setAttribute('aria-pressed', false);
      }
    }

    const track = window.stream.getVideoTracks()[0];
    const settings = track.getSettings();
    str = JSON.stringify(settings, null, 4);
    console.log('settings ' + str);

    //return navigator.mediaDevices.enumerateDevices();
  }

  function handleError(error) {
    console.log(error);

    //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    if (error === 'PermissionDeniedError') {
      alert('Permission denied. Please refresh and give permission.');
    }
  }
}

var BLOB;

function takeSnapshot() {
  // if you'd like to show the canvas add it to the DOM
  var canvas = document.createElement('canvas');

  var width = video.videoWidth;
  var height = video.videoHeight;

  canvas.width = width;
  canvas.height = height;

  context = canvas.getContext('2d');
  context.drawImage(video, 0, 0, width, height);

  // polyfil if needed https://github.com/blueimp/JavaScript-Canvas-to-Blob

  // https://developers.google.com/web/fundamentals/primers/promises
  // https://stackoverflow.com/questions/42458849/access-blob-value-outside-of-canvas-toblob-async-function
  function getCanvasBlob(canvas) {
    return new Promise(function(resolve, reject) {
      canvas.toBlob(function(blob) {
        BLOB = blob;
        var urlCreator = window.URL || window.webkitURL;
        var imageUrl = urlCreator.createObjectURL(blob);
        document.querySelector("#image").src = imageUrl;
        $("#menu").show();
        resolve(blob);
      }, 'image/jpeg');
    });
  }

  // some API's (like Azure Custom Vision) need a blob with image data
  getCanvasBlob(canvas).then(function(blob) {
    // do something with the image blob
  });
}

// https://hackernoon.com/how-to-use-javascript-closures-with-confidence-85cd1f841a6b
// closure; store this in a variable and call the variable as function
// eg. var takeSnapshotUI = createClickFeedbackUI();
// takeSnapshotUI();

function createClickFeedbackUI() {
  // in order to give feedback that we actually pressed a button.
  // we trigger a almost black overlay
  var overlay = document.getElementById('video_overlay'); //.style.display;


  var overlayVisibility = false;
  var timeOut = 80;

  function setFalseAgain() {
    overlayVisibility = false;
    overlay.style.display = 'none';
  }

  return function() {
    if (overlayVisibility == false) {
      overlayVisibility = true;
      overlay.style.display = 'block';
      setTimeout(setFalseAgain, timeOut);
    }
  };
}

function showCamera() {
  console.log("hi")
  $("#menu").hide();
}

function upload() {
  $("#loading").show();

  const s3 = new AWS.S3({
    region: "us-east-2",
    accessKeyId: "AKIAI545UQAERY45RGQA",
    secretAccessKey: "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"
  });
  
  var params = {   
    Bucket: "plateid-licence-plates", Key: "test.jpg" , Body: BLOB
  };  

  s3.putObject(params, function(err, data) {    
    if (err) 
      console.log(err, err.stack); // an error occurred    
    else {
      console.log(data);           // successful response    /*    
      $("#loading").hide();
      $("#done").show();
      document.querySelector("#image").src = '';
    }
  });
}

var SCAN_URL = "http://localhost:51077/scan/";
var INPUT_URL = "http://localhost:51077/inputinfo/";
var PATH_URL = "http://localhost:51077/path/";
var BASE_PATH = "";

function scanPlate() {
    var req = new XMLHttpRequest();
    req.open("PUT", SCAN_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        console.log(resp);
        if (resp.status == "success") {
            if (resp.message) {
                console.log(resp);
                warnUser(resp.message);
            } else {
                updateMainDoc();
            }
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send(JSON.stringify({})); // the request body would ideally contain the license plate string after image processing
}

function warnUser(message) {
    console.log('has message property');
    
    var criminalContainer = document.getElementById('criminalInfoBox');
    while (criminalContainer.lastChild) {
        criminalContainer.removeChild(criminalContainer.lastChild);
    }
    var alertDiv = document.createElement("div");
    alertDiv.setAttribute("role", "alert");
    alertDiv.setAttribute("class", "alert alert-warning alert-dismissible fade show");
    alertDiv.innerHTML = message;

    var closeBtn = document.createElement("button");
    closeBtn.setAttribute("type", "button");
    closeBtn.setAttribute("class", "close");
    closeBtn.setAttribute("data-dismiss", "alert");
    closeBtn.setAttribute("aria-label", "Close");

    var span = document.createElement("span");
    span.setAttribute("aria-hidden", "true");
    span.innerHTML = "&times";
    closeBtn.appendChild(span);
    alertDiv.appendChild(closeBtn);
    
    criminalContainer.appendChild(alertDiv);

    var generateTicket = document.getElementById("generateTicketForm");
    while (generateTicket.lastChild) {
        generateTicket.removeChild(generateTicket.lastChild);
    }
    var ticketButton = document.createElement("button");
    ticketButton.setAttribute("type", "button");
    ticketButton.setAttribute("class", "btn btn-info");
    ticketButton.innerHTML = "Generate Ticket";
    ticketButton.onmouseup = function() {
        window.open("/Users/joshuajohnson/Documents/software_dev_class/sdp-plateid-project-master/plate_scanner_ui/index.html");
    };
    generateTicket.appendChild(ticketButton);
}

function updateMainDoc() {
    var req = new XMLHttpRequest();
    req.open("GET", INPUT_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            var criminalContainer = document.getElementById('criminalInfoBox');
            while (criminalContainer.lastChild) {
                criminalContainer.removeChild(criminalContainer.lastChild);
            }
            var isFelon = resp.inputinfo.criminalinfo.isFelon;
            var alertDiv = document.createElement("div");
            alertDiv.setAttribute("role", "alert");
            if (isFelon) {
                alertDiv.setAttribute("class", "alert alert-danger alert-dismissible fade show");
                alertDiv.innerHTML = "Warning: this vehicle's owner is a felon.";
            } else {
                alertDiv.setAttribute("class", "alert alert-success alert-dismissible fade show");
                alertDiv.innerHTML = "This vehicle's owner has no criminal history.";
            }
            var closeBtn = document.createElement("button");
            closeBtn.setAttribute("type", "button");
            closeBtn.setAttribute("class", "close");
            closeBtn.setAttribute("data-dismiss", "alert");
            closeBtn.setAttribute("aria-label", "Close");
            var span = document.createElement("span");
            span.setAttribute("aria-hidden", "true");
            span.innerHTML = "&times";
            closeBtn.appendChild(span);
            alertDiv.appendChild(closeBtn);

            criminalContainer.appendChild(alertDiv);

            var generateTicket = document.getElementById("generateTicketForm");
            while (generateTicket.lastChild) {
                generateTicket.removeChild(generateTicket.lastChild);
            }
            var ticketButton = document.createElement("button");
            ticketButton.setAttribute("type", "button");
            ticketButton.setAttribute("class", "btn btn-info");
            ticketButton.innerHTML = "Generate Ticket";
            ticketButton.onmouseup = function() {
                window.open(BASE_PATH + "PopulateTicketForm/input/index.html");
            };
            generateTicket.appendChild(ticketButton);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}

window.onload = function() {
    var req = new XMLHttpRequest();
    req.open("GET", PATH_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            BASE_PATH = resp.path;
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}
