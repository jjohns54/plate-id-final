var INPUT_URL = "http://localhost:51077/inputinfo/";
var OUTPUT_URL = "http://localhost:51077/outputinfo/";
var PATH_URL = "http://localhost:51077/path/";
var BASE_PATH = "";

function showHideOther() {
    var vehicleType = $('input[name=vehicleType]:checked').val();
    if (vehicleType == "other") {
        $('#vehicleOtherType').css('display', 'inline-flex');
    } else {
        $('#vehicleOtherType').css('display', 'none');
    }
}

function sendOutputInfo(outputInfo) {
    var req = new XMLHttpRequest();
    req.open("POST", OUTPUT_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        console.log(resp);
        if (resp.status == "success") {
            window.open("/Users/joshuajohnson/Documents/software_dev_class/sdp-plateid-project-master/PopulateTicketForm/output/index.html");
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send(JSON.stringify(outputInfo));
}

function submitTicketForm() {
    var outputInfo = {};

    var ticketInformation = {
        court: $('#ticketCourt').val(),
        county: $('#ticketCounty').val(),
        state: ($('#ticketState').val() == "-") ? "" : $('#ticketState').val(),
        city: $('#ticketCity').val(),
        ticket_number: $('#ticketNumber').val(),
        case: $('#ticketCase').val()
    };
    console.log(ticketInformation);
    
    outputInfo.ticketInformation = ticketInformation;

    var defendantAddress = {
        name: $('#defName').val(),
        street: $('#defStreet').val(),
        city: $('#defCity').val(),
        state: ($('#defState').val() == "-") ? "" : $('#defState').val(),
        zip: $('#defZip').val()
    };
    console.log(defendantAddress);
    outputInfo.defendantAddress = defendantAddress;

    var endorsements = "";
    var endCdl = $('#endCdl').prop('checked') ? "CDL" : "";
    if (endCdl !== "") {
        endorsements = endCdl;
    }
    var endMc = $('#endMc').prop('checked') ? "MC" : "";
    if (endMc !== "") {
        if (endorsements !== "") {
            endorsements = endorsements + ", " + endMc;
        } else {
            endorsements = endMc;
        }
    }
    var endOther = $('#endOther').val();
    if (endOther !== "") {
        if (endorsements !== "") {
            endorsements = endorsements + ", " + endOther;
        } else {
            endorsements = endOther;
        }
    }

    var heightFt = $('#heightFt').val() ? $('#heightFt').val().toString() : "";
    var heightIn = $('#heightIn').val() ? $('#heightIn').val().toString() : "";
    var height = "";
    if ((heightFt !== "") && (heightIn !== "")) {
        height = heightFt + " Ft, " + heightIn + " In";
    }
    //var height = (heightFt == "" && heightIn == "") ? "" : (heightFt + " Ft, " + heightIn + " In");

    var defendantID = {
        driverLicenseId: $('#driverLicenseId').val(),
        birthDate: $('#birthDate').val() ? $('#birthDate').val().toString() : "",
        issueDate: $('#issueDate').val() ? $('#issueDate').val().toString() : "",
        driverLicenseState: ($('#driverLicenseState').val() == "-") ? "" : $('#driverLicenseState').val(),
        driverLicenseClass: $('#driverLicenseClass').val(),
        driverLicenseExp: $('#driverLicenseExp').val() ? $('#driverLicenseExp').val().toString() : "",
        endorsements: endorsements,
        ssn: $('#driverSSN').val(),
        height: height,
        weight: $('#driverWeight').val() ? $('#driverWeight').val().toString() : "",
        eyes: $('#driverEyes').val(),
        hair: $('#driverHair').val()
    };
    console.log(defendantID);
    outputInfo.defendantID = defendantID;

    var vehicleType = "";
    if ($('input[name=vehicleType]:checked').val() !== undefined) {
        if ($('input[name=vehicleType]:checked').val() == "other") {
            vehicleType = $('#vehicleOtherText').val();
        } else {
            vehicleType = $('input[name=vehicleType]:checked').val();
        }
    }

    var trafficViolationBackground = {
        vehicleType: vehicleType,
        year: $('#vehicleYear').val(),
        make: $('#vehicleMake').val(),
        model: $('#vehicleModel').val(),
        color: $('#vehicleColor').val(),
        plate_id: $('#vehiclePlate').val(),
        state: ($('#vehicleState').val() == "-") ? "" : $('#vehicleState').val(),
        highway: $('#stopHighway').val(),
        intersection: $('#stopIntersection').val(),
        city: $('#stopCity').val(),
        county: $('#stopCountyNum').val() ? $('#stopCountyNum').val().toString() : ""
    };
    console.log(trafficViolationBackground);
    outputInfo.background = trafficViolationBackground;

    var speeding = {
        value: $('#speeding-toggle').attr("aria-expanded")
    };
    if (speeding.value == "true") {
        speeding.overLimits = $('#overLimits').prop('checked') ? "Yes" : "No",
        speeding.unsafeSpeed = $('#unsafeSpeed').prop('checked') ? "Yes" : "No",
        speeding.driverSpeed = $('#driverSpeed').val() ? $('#driverSpeed').val().toString() : "",
        speeding.speedLimit = $('#speedLimit').val() ? $('#speedLimit').val().toString() : ""
    }

    var dui = {
        value: $('#dui-toggle').attr("aria-expanded")
    };
    if (dui.value == "true") {
        dui.underInfluence = $('#underInfluence').prop('checked') ? "Yes" : "No",
        dui.refusedBac = $('#refusedBac').prop('checked') ? "Yes" : "No",
        dui.driverBAC = $('#driverBAC').val()
    }
    console.log(dui);

    var offense_1 = {
        value: $('#otheroffense-toggle').attr("aria-expanded")
    };
    if (offense_1.value == "true") {
        offense_1.details = $('#offense-1-details').val();
    }
    console.log(offense_1);

    var offense_2 = {
        value: $('#otheroffense-toggle-2').attr("aria-expanded")
    };
    if (offense_2.value == "true") {
        offense_2.details = $('#offense-2-details').val();
    }
    console.log(offense_2);

    var offenses = {
        speeding: speeding,
        dui: dui,
        offense_1: offense_1,
        offense_2: offense_2
    };

    outputInfo.offenses = offenses;
    
    console.log(outputInfo);
    console.log(JSON.stringify(outputInfo));
    

    sendOutputInfo(outputInfo);
}

function preFillForm() {
    var req = new XMLHttpRequest();
    req.open("GET", INPUT_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            var plate_dict = resp.inputinfo.licenseplate;
            var owner_dict = resp.inputinfo.vehicleowner;
            $('#vehiclePlate').val(plate_dict.plate_id);
            $('#vehicleState').val(plate_dict.state);
            $('#driverSSN').val(owner_dict.ssn);
            $('#defName').val(owner_dict.firstName + " " + owner_dict.lastName);
            $('#defStreet').val(owner_dict.street);
            $('#defCity').val(owner_dict.city);
            $('#defState').val(owner_dict.state);
            $('#defZip').val(owner_dict.zipcode);
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}

window.onload = function() {
    preFillForm();

    var req = new XMLHttpRequest();
    req.open("GET", PATH_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            BASE_PATH = resp.path;
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}
