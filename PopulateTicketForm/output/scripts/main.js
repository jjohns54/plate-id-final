var OUTPUT_URL = "http://localhost:51077/outputinfo/";

class Item {
    constructor() {
    }
    addToDocument = function () {
        document.body.appendChild(this.item);
    };
    addElement = function (element) {
        this.item.appendChild(element.item);
    };
    setClass = function (className) {
        this.item.setAttribute("class", className);
    };
}

class TableElement {
    constructor(type, text, dict) {
        this.item = document.createElement(type);
        if (dict.class) {
            this.item.setAttribute('class', dict.class);
        }
        if (dict.colspan) {
            this.item.colSpan = dict.colspan;
        }
        if (dict.style) {
            this.item.setAttribute('style', dict.style);
        }
        this.item.innerHTML = text;
    }
}
TableElement.prototype = new Item();

function addSpeedingOffense(div, dict) {
    var heading = document.createElement('h6');
    heading.innerHTML = "SPEEDING";
    div.appendChild(heading);
    var table = document.createElement('table');
    table.setAttribute('class', 'table table-sm table-bordered text-center');
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    var rows = [];
    for (var i = 0; i < 3; ++i) {
        rows.push(document.createElement('tr'));
        switch (i) {
            case 0:
                var th = new TableElement('th', 'Over limits?', {class: 'text-center', colspan: '1', style: 'height:15px;line-height:15px;min-height:15px;'});
                var td = new TableElement('td', dict.overLimits, {colspan: '3', style: 'height:15px;line-height:15px;min-height:15px;'});
                rows[i].appendChild(th.item);
                rows[i].appendChild(td.item);
                break;
            case 1:
                var th = new TableElement('th', 'Unsafe for conditions?', {class: 'text-center', colspan: '1', style: 'height:15px;line-height:15px;min-height:15px;'});
                var td = new TableElement('td', dict.unsafeSpeed, {colspan: '3', style: 'height:15px;line-height:15px;min-height:15px;'});
                rows[i].appendChild(th.item);
                rows[i].appendChild(td.item);
                break;
            case 2:
                var th_1 = new TableElement('th', 'Driver Speed', {class: 'text-center', style: 'width: 20%;height:15px;line-height:15px;min-height:15px;'});
                var td_1 = new TableElement('td', dict.driverSpeed, {style: 'width: 30%;height:15px;line-height:15px;min-height:15px;'});
                var th_2 = new TableElement('th', 'Speed Limit', {class: 'text-center', colspan: '1', style: 'width: 20%;height:15px;line-height:15px;min-height:15px;'});
                var td_2 = new TableElement('td', dict.speedLimit, {style: 'width: 30%;height:15px;line-height:15px;min-height:15px;'});
                rows[i].appendChild(th_1.item);
                rows[i].appendChild(td_1.item);
                rows[i].appendChild(th_2.item);
                rows[i].appendChild(td_2.item);
                break;
        }
        tbody.appendChild(rows[i]);
    }
    div.appendChild(table);
}

function addDUIOffense(div, dict) {
    var heading = document.createElement('h6');
    heading.innerHTML = "DUI";
    div.appendChild(heading);
    var table = document.createElement('table');
    table.setAttribute('class', 'table table-sm table-bordered text-center');
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    var rows = [];
    for (var i = 0; i < 2; ++i) {
        rows.push(document.createElement('tr'));
        switch (i) {
            case 0:
                var th = new TableElement('th', 'Under influence?', {class: 'text-center', colspan: '1', style: 'height:15px;line-height:15px;min-height:15px;'});
                var td = new TableElement('td', dict.underInfluence, {colspan: '3', style: 'height:15px;line-height:15px;min-height:15px;'});
                rows[i].appendChild(th.item);
                rows[i].appendChild(td.item);
                break;
            case 1:
                var th_1 = new TableElement('th', 'Refused?', {class: 'text-center', style: 'width: 25%;height:15px;line-height:15px;min-height:15px;'});
                var td_1 = new TableElement('td', dict.refusedBac, {style: 'width: 25%;height:15px;line-height:15px;min-height:15px;'});
                var th_2 = new TableElement('th', 'BAC', {class: 'text-center', colspan: '1', style: 'width: 25%;height:15px;line-height:15px;min-height:15px;'});
                var td_2 = new TableElement('td', dict.driverBAC, {style: 'width: 25%;height:15px;line-height:15px;min-height:15px;'});
                rows[i].appendChild(th_1.item);
                rows[i].appendChild(td_1.item);
                rows[i].appendChild(th_2.item);
                rows[i].appendChild(td_2.item);
                break;
        }
        tbody.appendChild(rows[i]);
    }
    div.appendChild(table);
}

function addOtherOffense(div, details) {
    var heading = document.createElement('h6');
    heading.innerHTML = "OTHER";
    div.appendChild(heading);
    var table = document.createElement('table');
    table.setAttribute('class', 'table table-sm table-bordered text-center');
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    var row = document.createElement('tr');

    var th = new TableElement('th', 'Details', {class: 'text-center', style: 'width: 10%;line-height:15px;min-height:15px;'});
    var td = new TableElement('td', details, {style: 'line-height:15px;min-height:15px;'});
    row.appendChild(th.item);
    row.appendChild(td.item);
    tbody.appendChild(row);

    div.appendChild(table);
}


function generateTicket() {
    var req = new XMLHttpRequest();
    req.open("GET", OUTPUT_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            console.log(resp);
            var ticketInformation = resp.outputinfo.ticketInformation;
            var defendantAddr = resp.outputinfo.defendantAddress;
            var defendantID = resp.outputinfo.defendantID;
            var background = resp.outputinfo.background;
            var timestamp = resp.outputinfo.timestamp;
            var background = resp.outputinfo.background;
            var offenses = resp.outputinfo.offenses;

            $('#ticketCourt').text(ticketInformation.court);
            $('#ticketCounty').text(ticketInformation.county);
            $('#ticketState').text(ticketInformation.state);
            $('#ticketCity').text(ticketInformation.city);
            $('#ticketNumber').text(ticketInformation.ticket_number);
            $('#ticketCase').text(ticketInformation.case);

            $('#defName').text(defendantAddr.name);
            $('#defStreet').text(defendantAddr.street);
            $('#defCity').text(defendantAddr.city);
            $('#defState').text(defendantAddr.state);
            $('#defZip').text(defendantAddr.zip);

            $('#driverLicenseId').text(defendantID.driverLicenseId);
            $('#birthDate').text(defendantID.birthDate);
            $('#issueDate').text(defendantID.issueDate);
            $('#driverLicenseState').text(defendantID.driverLicenseState);

            $('#driverLicenseClass').text(defendantID.driverLicenseClass);
            $('#driverLicenseExp').text(defendantID.driverLicenseExp);
            $('#endorsements').text(defendantID.endorsements);
            $('#driverSSN').text(defendantID.ssn);

            $('#driverHeight').text(defendantID.height);
            $('#driverWeight').text(defendantID.weight);
            $('#driverEyes').text(defendantID.eyes);
            $('#driverHair').text(defendantID.hair);

            $('#timestamp').text(timestamp.day + ", " + timestamp.time);

            $('#vehicleType').text(background.vehicleType);
            $('#vehicleYear').text(background.year);
            $('#vehicleMake').text(background.make);
            $('#vehicleModel').text(background.model);
            $('#vehicleColor').text(background.color);
            $('#vehicleLicense').text(background.plate_id);
            $('#vehicleState').text(background.state);
            $('#stopHighway').text(background.highway);
            $('#stopIntersection').text(background.intersection);
            $('#stopCity').text(background.city);
            $('#stopCounty').text(background.county);
            $('#stopState').text(ticketInformation.state);

            var offensesDiv = document.getElementById('offenses');
            if (offenses.speeding.value == "true") {
                addSpeedingOffense(offensesDiv, offenses.speeding);
            }
            if (offenses.dui.value == "true") {
                addDUIOffense(offensesDiv, offenses.dui);
            }
            if (offenses.offense_1.value == "true") {
                addOtherOffense(offensesDiv, offenses.offense_1.details);
            }
            if (offenses.offense_2.value == "true") {
                addOtherOffense(offensesDiv, offenses.offense_2.details);
            }
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}

window.onload = function() {
    generateTicket();
}
