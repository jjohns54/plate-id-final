var SCAN_URL = "http://localhost:51077/scan/";
var INPUT_URL = "http://localhost:51077/inputinfo/";
var PATH_URL = "http://localhost:51077/path/";
var BASE_PATH = "";

function scanPlate() {
    var req = new XMLHttpRequest();
    req.open("PUT", SCAN_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        console.log(resp);
        if (resp.status == "success") {
            updateMainDoc();
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send(JSON.stringify({}));
}

function updateMainDoc() {
    var req = new XMLHttpRequest();
    req.open("GET", INPUT_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            var criminalContainer = document.getElementById('criminalInfoBox');
            while (criminalContainer.lastChild) {
                criminalContainer.removeChild(criminalContainer.lastChild);
            }
            var isFelon = resp.inputinfo.criminalinfo.isFelon;
            var alertDiv = document.createElement("div");
            alertDiv.setAttribute("role", "alert");
            if (isFelon) {
                alertDiv.setAttribute("class", "alert alert-danger alert-dismissible fade show");
                alertDiv.innerHTML = "Warning: this vehicle's owner is a felon.";
            } else {
                alertDiv.setAttribute("class", "alert alert-success alert-dismissible fade show");
                alertDiv.innerHTML = "This vehicle's owner has no criminal history.";
            }
            var closeBtn = document.createElement("button");
            closeBtn.setAttribute("type", "button");
            closeBtn.setAttribute("class", "close");
            closeBtn.setAttribute("data-dismiss", "alert");
            closeBtn.setAttribute("aria-label", "Close");
            var span = document.createElement("span");
            span.setAttribute("aria-hidden", "true");
            span.innerHTML = "&times";
            closeBtn.appendChild(span);
            alertDiv.appendChild(closeBtn);

            criminalContainer.appendChild(alertDiv);

            var generateTicket = document.getElementById("generateTicketForm");
            while (generateTicket.lastChild) {
                generateTicket.removeChild(generateTicket.lastChild);
            }
            var ticketButton = document.createElement("button");
            ticketButton.setAttribute("type", "button");
            ticketButton.setAttribute("class", "btn btn-info");
            ticketButton.innerHTML = "Generate Ticket";
            ticketButton.onmouseup = function() {
                window.open(BASE_PATH + "PopulateTicketForm/input/index.html");
            };
            generateTicket.appendChild(ticketButton);
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}

window.onload = function() {
    var req = new XMLHttpRequest();
    req.open("GET", PATH_URL, true);
    req.onload = function() {
        var resp = JSON.parse(req.responseText);
        if (resp.status == "success") {
            BASE_PATH = resp.path;
        } else {
            console.log(resp);
        }
    }
    req.onerror = function() {
        console.error(req.statusText);
    }
    req.send();
}
